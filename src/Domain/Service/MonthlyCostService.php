<?php

namespace MonthlyCostInvoice\Domain\Service;

use MonthlyCostInvoice\Domain\ValueObject\Product;

class MonthlyCostService
{
    public static function computeMonthlyCost(float $usage, Product $product): float
    {
        if ($usage < $product->minUsage()) {
            return self::computeMinCost($usage, $product);
        }

        if ($usage > $product->maxUsage()) {
            return self::computeMaxCost($usage, $product);
        }

        return self::computeNormalCost($usage, $product);
    }

    private static function computeMinCost(float $usage, Product $product): float
    {
        return $usage * $product->pricePerUnit() - ($usage * $product->minUsage() * 10 / 100);
    }

    private static function computeMaxCost(float $usage, Product $product): float
    {
        return $usage * $product->pricePerUnit() + ($usage * $product->minUsage() * 5 / 100);
    }

    private static function computeNormalCost(float $usage, Product $product): float
    {
        return $usage * $product->pricePerUnit();
    }
}
