<?php

namespace MonthlyCostInvoice\Domain\Model;

use MonthlyCostInvoice\Domain\ValueObject\Address;
use MonthlyCostInvoice\Domain\ValueObject\Product;

class Invoice
{
    /** Ar trebui sa aiba un identificator unic #id */
    private float $usage;
    private Product $product;
    private Address $address;
    private float $cost;

    private function __construct(float $usage, Product $product, Address $address, float $cost)
    {
        $this->usage = $usage;
        $this->product = $product;
        $this->address = $address;
        $this->cost = $cost;
    }

    public static function create(float $usage, Product $product, Address $address, float $cost): Invoice
    {
        return new self($usage, $product, $address, $cost);
    }

    public function cost(): float
    {
        return $this->cost;
    }

    public function usage(): float
    {
        return $this->usage;
    }

    public function product(): Product
    {
        return $this->product;
    }

    public function address(): Address
    {
        return $this->address;
    }
}
