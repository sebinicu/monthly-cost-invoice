<?php

namespace MonthlyCostInvoice\Domain\ValueObject;

interface Product
{
    public function type(): string;
    public function pricePerUnit(): int;
    public function minUsage(): int;
    public function maxUsage(): int;
}
