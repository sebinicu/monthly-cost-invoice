<?php

namespace MonthlyCostInvoice\Domain\ValueObject;

class Heat implements Product
{
    public const MIN_USAGE = 50;
    public const MAX_USAGE = 700;
    public const PRICE_PER_UNIT = 50;
    public const TYPE = 'Heat';

    public static function create(): Product
    {
        return new self();
    }

    public function type(): string
    {
        return self::TYPE;
    }

    public function pricePerUnit(): int
    {
        return self::PRICE_PER_UNIT;
    }

    public function minUsage(): int
    {
        return self::MIN_USAGE;
    }

    public function maxUsage(): int
    {
        return self::MAX_USAGE;
    }
}