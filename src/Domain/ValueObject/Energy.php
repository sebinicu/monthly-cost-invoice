<?php

namespace MonthlyCostInvoice\Domain\ValueObject;

class Energy implements Product
{
    public const MIN_USAGE = 100;
    public const MAX_USAGE = 2000;
    public const PRICE_PER_UNIT = 30;
    public const TYPE = 'Energy';

    public static function create(): Product
    {
        return new self();
    }

    public function type(): string
    {
        return self::TYPE;
    }

    public function pricePerUnit(): int
    {
        return self::PRICE_PER_UNIT;
    }

    public function minUsage(): int
    {
        return self::MIN_USAGE;
    }

    public function maxUsage(): int
    {
        return self::MAX_USAGE;
    }
}