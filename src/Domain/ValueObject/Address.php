<?php

namespace MonthlyCostInvoice\Domain\ValueObject;

class Address
{
    private string $type;
    private string $street;
    private int $number;
    private string $complement;
    private int $zipcode;
    private string $city;
    private string $country;

    private function __construct(string $type, string $street, int $number, string $complement, int $zipcode, string $city, string $country)
    {
        $this->type = $type;
        $this->street = $street;
        $this->number = $number;
        $this->complement = $complement;
        $this->zipcode = $zipcode;
        $this->city = $city;
        $this->country = $country;
    }

    public static function createFromArray($address): self
    {
        return new self(
            $address['type'],
            $address['street'],
            $address['number'],
            $address['complement'],
            $address['zipcode'],
            $address['city'],
            $address['country']
        );
    }

    public function type(): string
    {
        return $this->type;
    }

    public function street(): string
    {
        return $this->street;
    }

    public function number(): int
    {
        return $this->number;
    }

    public function complement(): string
    {
        return $this->complement;
    }

    public function zipcode(): int
    {
        return $this->zipcode;
    }

    public function city(): string
    {
        return $this->city;
    }

    public function country(): string
    {
        return $this->country;
    }
}
