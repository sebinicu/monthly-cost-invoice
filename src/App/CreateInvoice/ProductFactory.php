<?php

namespace MonthlyCostInvoice\App\CreateInvoice;

use MonthlyCostInvoice\Domain\ValueObject\Energy;
use MonthlyCostInvoice\Domain\ValueObject\Heat;
use MonthlyCostInvoice\Domain\ValueObject\Product;
use MonthlyCostInvoice\Domain\ValueObject\Water;
use RuntimeException;

class ProductFactory
{
    public static function create(string $productType): Product
    {
        switch ($productType) {
            case Water::TYPE:
                return Water::create();
            case Heat::TYPE:
                return Heat::create();
            case Energy::TYPE:
                return Energy::create();
            default:
                throw new RuntimeException(sprintf("Invalid product type: '%s'", $productType));
        }
    }
}
