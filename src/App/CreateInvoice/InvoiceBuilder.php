<?php

namespace MonthlyCostInvoice\App\CreateInvoice;

use MonthlyCostInvoice\Domain\Model\Invoice;
use MonthlyCostInvoice\Domain\Service\MonthlyCostService;
use MonthlyCostInvoice\Domain\ValueObject\Address;

class InvoiceBuilder
{
    public static function build($data): Invoice
    {
        $usage = $data['usage'];
        $product = ProductFactory::create($data['product_type']);
        $address = Address::createFromArray($data['address']);
        $cost = MonthlyCostService::computeMonthlyCost($usage, $product);

        return Invoice::create($usage, $product, $address, $cost);
    }
}
