<?php

namespace unit;

use MonthlyCostInvoice\App\CreateInvoice\InvoiceBuilder;
use MonthlyCostInvoice\Domain\ValueObject\Water;
use PHPUnit\Framework\TestCase;

class InvoiceTest extends TestCase
{
    public array $request;

    public function setUp(): void
    {
        $this->request = [
            'usage' => 200,
            'product_type' => 'Water',
            'address' => [
                'type' => 'postal',
                'street' => 'Square',
                'number' => 7,
                'complement' => '',
                'zipcode' => 77777,
                'city' => 'Bucharest',
                'country' => 'RO'
            ]
        ];
    }

    /** @test  */
    public function itCreateInvoiceFromRequest()
    {
        $invoice = InvoiceBuilder::build($this->request);
        self::assertEquals(200, $invoice->usage());
        self::assertEquals(Water::TYPE, $invoice->product()->type());
        self::assertEquals('postal', $invoice->address()->type());
        self::assertEquals('Square', $invoice->address()->street());
        self::assertEquals(7, $invoice->address()->number());
        self::assertEquals('', $invoice->address()->complement());
        self::assertEquals(77777, $invoice->address()->zipcode());
        self::assertEquals('Bucharest', $invoice->address()->city());
        self::assertEquals('RO', $invoice->address()->country());
    }

    /** @test  */
    public function itComputeMonthlyCostCorrectly()
    {
        $invoice = InvoiceBuilder::build($this->request);
        self::assertEquals(2000, $invoice->cost());
    }
}
